/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.controller;

import br.una.ads.projetoaplicadouna.helper.GeradorDeNumeros;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelResultadoAproximado;
import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author william
 */
public class ResultadoAproximado {
    
    public List<Integer> gerarResultadoAproximado(int resultado){
        
        Nivel nivel = new NivelResultadoAproximado(resultado);
        
        List<Integer> resultadoAproximado = new ArrayList<>();
        
        int aproximado1, aproximado2;
        aproximado1 = GeradorDeNumeros.getInstance().gerar(nivel);
        aproximado2 = GeradorDeNumeros.getInstance().gerar(nivel);
        
        while(aproximado1 == resultado || aproximado2 == resultado || aproximado1 == aproximado2){
            
            if(aproximado1 == resultado){
                aproximado1 = GeradorDeNumeros.getInstance().gerar(nivel);                
            }
            
            if(aproximado2 == resultado){
                aproximado2 = GeradorDeNumeros.getInstance().gerar(nivel);                
            }
            
            if(aproximado1 == aproximado2){
                aproximado1 = GeradorDeNumeros.getInstance().gerar(nivel);                
            }
            
        }
        
        resultadoAproximado.add(aproximado1);
        resultadoAproximado.add(aproximado2);
        return resultadoAproximado;
    }
    
}
