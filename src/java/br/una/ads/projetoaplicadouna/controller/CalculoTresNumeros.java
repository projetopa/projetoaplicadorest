/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.controller;

import br.una.ads.projetoaplicadouna.helper.GeradorDeNumeros;
import br.una.ads.projetoaplicadouna.modelo.CalculoMatematico;
import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;
import br.una.ads.projetoaplicadouna.modelo.interfaces.OperacoesMatematicas;
import br.una.ads.projetoaplicadouna.modelo.interfaces.TipoDeCalculo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author william
 */
public class CalculoTresNumeros implements TipoDeCalculo{
    private Nivel nivel;

    public CalculoTresNumeros(Nivel nivel) {
        this.nivel = nivel;
    }
    
    

    @Override
    public CalculoMatematico gerar(OperacoesMatematicas operacao) {
        
        int valor1,valor2,valor3;
        valor1 = GeradorDeNumeros.getInstance().gerar(nivel);
        valor2 = GeradorDeNumeros.getInstance().gerar(nivel);               
        valor3 = GeradorDeNumeros.getInstance().gerar(nivel); 
        
        while(valor1 <= 3){
            valor1 = GeradorDeNumeros.getInstance().gerar(nivel);
        }            
        
        if(valor2 > valor1 ){
            int vl = valor1;
            valor1 = valor2;
            valor2 = vl;
        }    
         
        if(valor3 > valor2 ){
            int vl = valor2;
            valor2 = valor3;
            valor3 = vl;
        }      
        
        CalculoMatematico algarismo = new CalculoMatematico();
        List<Integer> valores = new ArrayList<>();
        valores.add(valor1);
        valores.add(valor2);
        valores.add(valor3);
        algarismo.setValores(valores);
        
        return operacao.gerarCalculo(algarismo);
    }
    
}
