/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.controller.Factory;

import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelCinco;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelDez;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelDois;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelNove;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelOito;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelQuatro;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelSeis;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelSete;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelTres;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelUm;

/**
 *
 * @author william
 */
public class NivelFactory {
    
    public Nivel getNivel(int level){
        
        Nivel nivel = null;

        switch (level) {
            case 1:
                nivel = new NivelUm();
                break;
            case 2:
                nivel = new NivelDois();
                break;
            case 3:
                nivel = new NivelTres();
                break;
            case 4:
                nivel = new NivelQuatro();
                break;
            case 5:
                nivel = new NivelCinco();
                break;
            case 6:
                nivel = new NivelSeis();
                break;
            case 7:
                nivel = new NivelSete();
                break;
            case 8:
                nivel = new NivelOito();
                break;
            case 9:
                nivel = new NivelNove();
                break;
            case 10:
                nivel = new NivelDez();
                break;                
            default:
                nivel = new NivelUm();
                break;
        }
        
        return nivel;
        
    }
    
}
