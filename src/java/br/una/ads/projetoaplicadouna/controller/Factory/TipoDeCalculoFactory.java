/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.controller.Factory;

import br.una.ads.projetoaplicadouna.modelo.interfaces.TipoDeCalculo;
import br.una.ads.projetoaplicadouna.controller.CalculoDoisNumeros;
import br.una.ads.projetoaplicadouna.controller.CalculoDoisNumeros;
import br.una.ads.projetoaplicadouna.controller.CalculoQuatroNumeros;
import br.una.ads.projetoaplicadouna.controller.CalculoTresNumeros;
import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;

/**
 *
 * @author william
 */
public class TipoDeCalculoFactory {

    public TipoDeCalculo getTipoDeCalculo(int tipo, Nivel nivel) {

        TipoDeCalculo tipoDeCalculo;

        switch (tipo) {
            case 2:
                tipoDeCalculo = new CalculoDoisNumeros(nivel);
                break;
            case 3:
                tipoDeCalculo = new CalculoTresNumeros(nivel);
                break;
            case 4:
                tipoDeCalculo = new CalculoQuatroNumeros(nivel);
                break;
            default:
                tipoDeCalculo = new CalculoDoisNumeros(nivel);
                break;
        }

        return tipoDeCalculo;
    }

}
