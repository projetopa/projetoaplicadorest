/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.controller.operacoes;

import br.una.ads.projetoaplicadouna.controller.ResultadoAproximado;
import br.una.ads.projetoaplicadouna.modelo.CalculoMatematico;
import br.una.ads.projetoaplicadouna.modelo.interfaces.OperacoesMatematicas;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author william
 */
public class Subtracao implements OperacoesMatematicas {

    // TRATAR PARA QUE O RESULTADO NÃO SEJE NEGATIVO
    
    @Override
    public CalculoMatematico gerarCalculo(CalculoMatematico calculoMatematico) {

                
        List<Integer> lista = calculoMatematico.getValores();        
        
        int resultado = lista.get(0); 
        
        for(int i = 1; i < lista.size(); i++){
            
            if(i == 2 || i == 3){
               if(resultado == 1){
                   lista.set(i, 1);                   
               }
               else if(resultado == 0){
                   lista.set(i, 0);
               }
               else if(resultado < lista.get(i)){
                   lista.set(i, 1);
               }
            }
            
            resultado = resultado - lista.get(i);
        }                        
        
        calculoMatematico.setDemaisAlternativas(new ResultadoAproximado().gerarResultadoAproximado(resultado));
        calculoMatematico.setResultado(resultado);
        return calculoMatematico;
    }
}
