/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.controller.operacoes;

import br.una.ads.projetoaplicadouna.controller.ResultadoAproximado;
import br.una.ads.projetoaplicadouna.helper.GeradorDeNumeros;
import br.una.ads.projetoaplicadouna.modelo.CalculoMatematico;
import br.una.ads.projetoaplicadouna.modelo.niveis.NivelResultadoAproximado;
import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;
import br.una.ads.projetoaplicadouna.modelo.interfaces.OperacoesMatematicas;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author William
 */
public class Soma implements OperacoesMatematicas{    
    
    @Override
    public CalculoMatematico gerarCalculo(CalculoMatematico calculoMatematico) {                
        
        int resultado = 0; 
        
        for(int i = 0; i < calculoMatematico.getValores().size(); i++){
            resultado += calculoMatematico.getValores().get(i);
        }                        
        
        calculoMatematico.setDemaisAlternativas(new ResultadoAproximado().gerarResultadoAproximado(resultado));
        calculoMatematico.setResultado(resultado);
        return calculoMatematico;
    }
    
    
}
