/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.servico;

import br.una.ads.projetoaplicadouna.controller.Factory.NivelFactory;
import br.una.ads.projetoaplicadouna.controller.Factory.TipoDeCalculoFactory;
import br.una.ads.projetoaplicadouna.controller.operacoes.Multiplicacao;
import br.una.ads.projetoaplicadouna.controller.operacoes.Soma;
import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;
import br.una.ads.projetoaplicadouna.modelo.interfaces.TipoDeCalculo;
import com.google.gson.Gson;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author william
 */
@Path("/multiplicacao")
public class ServicoMultiplicacao {
    
     @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response retornaCalculo(@QueryParam("tipo") int tipo, @QueryParam("level") int level) {
             
        Nivel nivel = new NivelFactory().getNivel(level);
        TipoDeCalculo tipoDeCalculo = new TipoDeCalculoFactory().getTipoDeCalculo(tipo,nivel);        
        try {            
            String json = new Gson().toJson(tipoDeCalculo.gerar(new Multiplicacao()));

            return Response.status(Response.Status.OK).entity(json).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
