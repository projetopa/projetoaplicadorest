/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.modelo.niveis;

import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;

/**
 *
 * @author william
 */
public class NivelResultadoAproximado implements Nivel {

    private int resultado;

    public NivelResultadoAproximado(int resultado) {
        this.resultado = resultado;
    }
    
    
    
    @Override
    public int[] retornaContagem() {
        
        int[] valoresProximos = {(resultado - 3),(resultado + 3)};
            return valoresProximos;
    }
    
}
