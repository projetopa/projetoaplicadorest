/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.modelo.niveis;

import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;

/**
 *
 * @author william
 */
public class NivelDois implements Nivel{

    @Override
    public int[] retornaContagem() {
        int[] valores = {11,20};
        return valores;
    }
    
}
