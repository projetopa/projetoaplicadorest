/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.modelo.interfaces;

import br.una.ads.projetoaplicadouna.modelo.CalculoMatematico;

/**
 *
 * @author William
 */
public interface OperacoesMatematicas {
    
    public CalculoMatematico gerarCalculo(CalculoMatematico calculoMatematico);
}
