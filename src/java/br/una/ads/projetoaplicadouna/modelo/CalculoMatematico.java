/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.modelo;

import java.util.List;

/**
 *
 * @author william
 */
public class CalculoMatematico  {
    
    private List<Integer> valores;
    private Integer resultado;
    private List<Integer> demaisAlternativas;

    public List<Integer> getValores() {
        return valores;
    }

    public void setValores(List<Integer> valores) {
        this.valores = valores;
    }
    

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }

    public List<Integer> getDemaisAlternativas() {
        return demaisAlternativas;
    }

    public void setDemaisAlternativas(List<Integer> demaisAlternativas) {
        this.demaisAlternativas = demaisAlternativas;
    }
            
}
