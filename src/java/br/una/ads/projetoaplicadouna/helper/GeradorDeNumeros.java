/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.una.ads.projetoaplicadouna.helper;

import br.una.ads.projetoaplicadouna.modelo.interfaces.Nivel;
import java.util.Random;

/**
 *
 * @author william
 */
public class GeradorDeNumeros {
    
    private static GeradorDeNumeros geradorDeNumeros;    
    
    
    private GeradorDeNumeros(){}
    
    public static synchronized GeradorDeNumeros getInstance(){
        
        if(geradorDeNumeros == null) geradorDeNumeros = new GeradorDeNumeros();
        
        return geradorDeNumeros;
    }
    
    public int gerar(Nivel nivel){
        
        int inicio = 0, fim = 0;
        inicio = nivel.retornaContagem()[0];
        fim = nivel.retornaContagem()[1];
        
        Random gerador = new Random();
        
        int valor = gerador.nextInt(fim);
        
        while(valor < inicio || valor > fim){
            
            if(valor > inicio && valor < fim) break;
            
            valor = gerador.nextInt(fim);
        }
        
        return valor;
    }
    
    
    
}
